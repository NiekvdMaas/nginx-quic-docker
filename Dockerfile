FROM archlinux:base-devel

RUN set -x \
    && groupadd --gid 101 --system nginx \
    && useradd --uid 101 --gid nginx --system --create-home --home-dir /var/cache/nginx --shell /sbin/nologin nginx \
    && echo "nginx ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/nginx \
    && pacman --noconfirm -Sy git \
    && mkdir -m 777 /aur \
    && sed -Ei 's/^#MAKEFLAGS="-j2"/MAKEFLAGS="-j$(nproc)"/' /etc/makepkg.conf \
    && su nginx -s /bin/sh -c ' \
        cd /aur \
        && git clone https://aur.archlinux.org/nginx-quic.git \
        && cd nginx-quic \
        && binarypkg=$(makepkg --packagelist | head -1) \
        && makepkg -scr --noconfirm \
        && sudo pacman --noconfirm -U $binarypkg \
    ' \
    && rm -f /etc/sudoers.d/nginx \
    && rm -rf /aur \
    && rm -rf /var/cache/pacman/pkg/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

COPY nginx.conf /etc/nginx/nginx.conf

COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 80
EXPOSE 443/tcp
EXPOSE 443/udp

STOPSIGNAL SIGQUIT

# Generate shared self-signed HTTPS to demonstrate QUIC capatibilities
CMD ["nginx", "-g", "daemon off;"]
