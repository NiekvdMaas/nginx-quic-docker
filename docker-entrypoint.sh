#!/bin/sh

set -e

KEY_LOCATION=${KEY_LOCATION:-"/etc/nginx/cert.key"}
CERT_LOCATION=${CERT_LOCATION:-"/etc/nginx/cert.pem"}
CERT_USE_EC=${CERT_USE_EC:-true}

if [ ! -f "$KEY_LOCATION" ] && [ ! -f "$CERT_LOCATION" ]; then
    echo "$(basename $0): info: no certificate at default locations /etc/nginx/cert.key and /etc/nginx/cert.pem found"
    if [ $CERT_USE_EC = true ]; then
        echo "$(basename $0): info: generating self-signed P-256 elliptic curve certificate"
        openssl req -x509 -newkey ec -pkeyopt ec_paramgen_curve:P-256 \
         -days 365 -subj '/CN=localhost/O=localhost/C=US' -nodes -keyout /etc/nginx/cert.key -out /etc/nginx/cert.pem
    else
        echo "$(basename $0): info: generating self-signed RSA certificate"
        openssl req -x509 -newkey rsa:4096 \
         -days 365 -subj '/CN=localhost/O=localhost/C=US' -nodes -keyout /etc/nginx/cert.key -out /etc/nginx/cert.pem
    fi
    echo "Your certificate fingerprint is:"
    openssl x509 -in /etc/nginx/cert.pem -pubkey -noout | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | openssl enc -base64
    echo "Use with e.g. the Chrome flag --ignore-certificate-errors-spki-list to whitelist this fingerprint"
fi

exec "$@"
